<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class PageNotFoundController extends AbstractController
{
    /**
     * @return Response
     */
    public function pageNotFoundAction(): Response
    {
        return new JsonResponse(array('result' => 'Page not found'), 404);
    }
}
