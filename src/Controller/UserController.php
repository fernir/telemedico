<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends MyAbstractController
{
    private $passwordEncoder;
    /**
     * UserController constructor.
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/api/user/getMyProfile", name="get_my_profile", methods={"GET"})
     */
    public function profileAction()
    {
        return $this->json(array('user' => $this->getUser()), 200, array(), array('groups' => 'api'));
    }

    // create new user action is in LoginController as register POST

    /**
     * @Route("/api/user/edit", name="api_user_edit", methods={"PATCH"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function editUserAction(Request $request)
    {
        /** @var User|null $user */
        $user = $this->getUser();
        try {
            $this->editUser($user, $request);
        } catch (\Throwable $exception) {
            return $this->json(['error' => $exception->getMessage()], 400);
        }
        return $this->json(array('user' => $this->getUser()), 200, array(), array('groups' => 'api'));
    }

    /**
     * @Route("/api/user/edit/{id}", name="api_user_edit_by_id", methods={"PATCH"})
     *
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function editUserByIdAction(Request $request, int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this method!');

        $user = $this->getUserById($id);
        if (!$user) {
            return $this->json(['error' => 'User could not be found.'], 400);
        }
        try {
            $editedUser = $this->editUser($user, $request);
        } catch (\Throwable $exception) {
            return $this->json(['error' => $exception->getMessage()], 400);
        }
        return $this->json(array('user' => $editedUser), 200, array(), array('groups' => 'api'));

    }

    /**
     * @Route("/api/user/delete", name="api_user_delete", methods={"DELETE"})
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteMyProfileAction(Request $request)
    {
        $user = $this->getUser();
        $this->entityManager->remove($user);
        $this->entityManager->flush();
        //logout
        return $this->json(array('result' => true));
    }

    /**
     * @Route("/api/user/delete/{id}", name="api_user_delete_by_id", methods={"DELETE"})
     *
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteUserAction(Request $request, int $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Unable to access this method!');

        $this->getUserById($id);
        $user = $this->getUser();
        $this->entityManager->remove($user);
        $this->entityManager->flush();
        return $this->json(array('result' => true));
    }

    /**
     * @param User $user
     * @param Request $request
     * @return User
     * @return \Throwable
     */
    private function editUser(User $user, Request $request)
    {
        $form = $this->createForm(
            UserType::class,
            $user,
            array('csrf_protection' => false, 'allow_extra_fields' => true)
        );

        $form->submit(json_decode($request->getContent(), true));
        $form->isValid();

        $data = json_decode($request->getContent(), true);

        $user->setPassword($this->passwordEncoder->encodePassword($user, $data['password']));
        // ... set other User atributes
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        return $user;
    }

    /**
     * @param int $id
     * @return User|null
     */
    private function getUserById(int $id)
    {
        /** @var User|null $user */
        $user = $this->entityManager->getRepository(User::class)->find($id);
        return $user;
    }
}
