<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

class HomeController extends MyAbstractController
{
    /**
     * @Route("/", name="api_home")
     */
    public function home()
    {
        return $this->json(['result' => true]);
    }
}
