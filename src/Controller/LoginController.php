<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginController extends MyAbstractController
{
    /**
     * @Route("/api/login", name="api_login", methods={"POST"})
     */
    public function login()
    {
        return $this->json(['result' => true]);
    }

    /**
     * @Route("/api/register", name="api_register", methods={"POST"})
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function register(UserPasswordEncoderInterface $passwordEncoder, Request $request)
    {
        try {
            $user = new User();
            $form = $this->createForm(
                UserType::class,
                $user,
                array('csrf_protection' => false, 'allow_extra_fields' => true)
            );

            $form->submit($request->request->all());

            $user->setEmail($request->request->get('email'));
            // tutaj walidacja hasła
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $this->entityManager->persist($user);
            $this->entityManager->flush();

            return $this->json(['result' => true, 'user' => $user], 200, array(), array('groups' => 'api'));
        } catch(UniqueConstraintViolationException $e) {
            return $this->json(['error' => 'The email provided already has an account!'], 400);
        } catch (\Throwable $exception) {
            dump($exception);
            die();
            // return $this->json(['error' => 'Unable to save new user.'], 400);
            return $this->json(['error' => $exception->getMessage()], 400);
        }
    }
}
